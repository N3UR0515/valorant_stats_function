import requests, json
from bs4 import BeautifulSoup
valorant_stats  = []
def get_valorant_stats(username: str) -> str:
    """Returns the Valorant stats for a specified user in json format.

    Args:
        username (str): A string combination of user and tagline separated by a #, formatted as username#tagline.

    Returns:
        dict: A json dictionary variable containing the user's stats, or an error code.

    Example:
        >>> get_stats(username#tagline)
        [
            {
                "Rank": "Bronze 3"
            },
            {
                "Icon": "https://imgsvc.trackercdn.com/url/max-width(96),quality(66)/https%3A%2F%2Ftrackercdn.com%2Fcdn%2Ftracker.gg%2Fvalorant%2Ficons%2Ftiersv2%2F8.png/image.png"
            },
            {
                "Peak Rating": "Gold 2"
            },
            {
                "Damage/Round": "184.7"
            },
            {
                "K/D Ratio": "1.33"
            },
            {
                "Headshot %": "27.6%"
            },
            {
                "Win %": "47.8%"
            },
            {
                "Wins": "11"
            },
            {
                "KAST": "75.3%"
            },
            {
                "DD\u0394/Round": "45"
            },
            {
                "Kills": "419"
            },
            {
                "Deaths": "315"
            },
            {
                "Assists": "113"
            },
            {
                "ACS": "269.7"
            }
        ]  # This is an example output and may vary each time the function is called.
    """
    stats_info = []
            # Splitting the username and tagline
    user = username.split("#")[0]
    tagline = username.split("#")[1]
    URL = f"https://tracker.gg/valorant/profile/riot/{user}%23{tagline}/overview"
    page = requests.get(URL)
    soup = BeautifulSoup(page.content, "html.parser")
    results = soup.find()
    rank = results.find_all("div", class_="trn-profile-highlighted-content__stats")
    peak = results.find("div", class_="rating-summary__content rating-summary__content--secondary")

    peak_rating = peak.find("div", class_="value").text.strip()

    for r in rank:
        rank = r.find("span", class_="stat__value")
        icon = r.find("img", class_="trn-profile-highlighted-content__icon")

        stats_info.append({
                "Rank": rank.text.strip()
            })

        stats_info.append({
            "Icon": icon["src"]
        })

    stats_info.append({
            "Peak Rating": peak_rating
        })

    stats = results.find_all("div", class_="numbers")

    for stat in stats:
        names = stat.find("span", class_="name")
        values = stat.find("span", class_="value")
        for name in names:
            name = name.text.strip()

        for value in values:
            value = value.text.strip()

        stats_info.append({
                name: value
            })
    try:
        valorant_stats = json.dumps(stats_info)
        #print(valorant_stats)
        return valorant_stats
    except IOError as e:
        print(f"An error occurred: {e}")
        return None

# Example usage
# get_valorant_stats("Username#Tagline")