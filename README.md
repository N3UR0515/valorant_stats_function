# valorant_stats_function

## Description

This is a function that will return the stats of a player in Valorant by scraping their statistics from [Tracker.GG](https://tracker.gg/valorant). It will return the player's rank, peak rating, and the number of wins they have, along with a few other helpful stats.

## Usage

Import the function into your code and call it with the player's name as the argument. The function will return a dictionary with the player's stats.

Example: get_valorant_stats("YourName#YourTag")

!Note: To print the stats to screen, simply uncomment this line `#print(valorant_stats)` by removing the `#` at the beginning of the line.
